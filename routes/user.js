const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js')
const auth = require('../auth.js');

// Get All User Route
router.get('/all', (req, res) => {
    userControllers.getAllUsers().then(resultFromController => res.send(resultFromController));
})

// User Registration Route
router.post('/register', (req, res) => {
    userControllers.registerUser(req.body).then(resultFromController => {
        res.send(resultFromController);
    })
});

// User Authentication
router.post('/userAuth', (req, res) => {
    userControllers.authenticateUser(req.body).then(resultFromController => 
        res.send(resultFromController)
    );
});

// This is to connect the route to other modules.
module.exports = router;