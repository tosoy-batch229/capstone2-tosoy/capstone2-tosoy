const jwt = require('jsonwebtoken');

// Token Creation
const secret = `DeliciousNoodlesAPI`;
module.exports.createAccessTokens = (user) => {
    const data = {
        id: user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }
    return jwt.sign(data, secret, {});
};

// Token Verification
module.exports.verify = (req, res, next) => {
    let token = req.headers.authorization;
    // console.log(token);

    if (typeof token != 'undefined') {
        // console.log(token);

        // This will remove the Bearer prefix.
        token = token.slice(7, token.length);

        // Validate the token using the verify method by decrypting the token using the secret code.
        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return res.send({auth: `Authentication Failed`});
            } else {
                next();
            }
        })
    } else {
        return res.send({auth: `Token does not exist.`});
    }
}

// Token Decryption
module.exports.decode = (token) => {
    if (typeof token !== `undefined`) {
        token = token.slice(7, token.length);

        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return null;
            } else {
                return jwt.decode(token, {complete : true}).payload;
            }
        })
    } else {
        return null;
    }
};