const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    email : {
        type : String, 
        required : [true, `Email is required!`]
    },
    password : {
        type : String,
        required : [true, `Password is required!`]
    },
    isAdmin : {
        type : Boolean,
        default : false
    }, 
    orders : [{
        products : [{
            productName : {
                type : String,
                required : [true, `Product name is Required`]
            },
            quantity : {
                type : Number,
                required : [true, `Quantity is required!`]
            }
        }]
    }]
})

module.exports = mongoose.model('Users', userSchema);