const Users = require('../models/Users.js');
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// View All User
module.exports.getAllUsers = () => {
    return Users.find({}).then(result => {
        result = result.map((user) => {
            return {
                email : user.email,
                password : "",
                isAdmin : user.isAdmin
            }
        })
        return result;
    });
};

// Register A New User
module.exports.registerUser = (reqBody) => {
    let newUser = new Users({
        email : reqBody.email,
        isAdmin : reqBody.isAdmin,
        password : bcrypt.hashSync(reqBody.password, 10) // -> '10' is the value of salt.
    })

    return newUser.save().then((user, error) => {
        if (error) {
            return false;
        } else {
            return true;
        }
    })
}

// User Authentication
module.exports.authenticateUser = (reqBody) => {
    return Users.findOne({email: reqBody.email}).then(result => {
        if (result == null) {
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return {access: auth.createAccessTokens(result)};
            } else {
                return false;
            }
        }
    })
};